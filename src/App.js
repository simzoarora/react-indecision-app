
// app.js
import React, { Component } from 'react';
import './App.css';  
import Counter from './playground/counter-example';
// import MainForm from './components/MainForm';
import { Container } from 'semantic-ui-react';

class App extends Component {

  render() {
    return(
      <Container textAlign='center'>
        <Counter />
      </Container>     )  
  }
}

export default App;