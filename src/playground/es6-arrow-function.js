const square = function (x){
    return x*x;
};
console.log(square(8));

const squareArrow = (x) =>{
    return x*x;
}
console.log(squareArrow(9));